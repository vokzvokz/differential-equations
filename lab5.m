clear;
graphics_toolkit("gnuplot");
okno5=figure();
hold on;

function x = solution(t)
  x = t^(3/2)/sqrt(t^5+t^3+2);
endfunction

function f = func(t, x)
  f = (3*x-5*t*t*x*x*x-3*x*x*x)/(2*t);  
endfunction
#
function F = euler(x0, a, b, h)
  t = a:h:b;
  x(1) = x0;
  for i = 1:1:(length(t)-1)
    x(i+1)=x(i)+h*func(t(i),x(i));
  endfor
  F(1, :) = t;
  F(2, :) = x;
endfunction
#
function F = runge(x0, a, b, h)
  t = a:h:b;
  x(1) = x0;
  for i = 1:1:(length(t)-1)
    k1 = func(t(i),x(i));
    k2 = func(t(i)+h/2,x(i)+h*k1/2);
    k3 = func(t(i)+h/2,x(i)+h*k2/2);
    k4 = func(t(i)+h,x(i)+h*k3);
    x(i+1)=x(i)+h*(k1+2*k2+2*k3+k4)/6;
  endfor
  F(1, :) = t;
  F(2, :) = x;
endfunction
#
function e = calculateError(x0,a,b,h,method)
  p = 1;
  if method=="euler"
    F1 = euler(x0,a,b,h);
    F2 = euler(x0,a,b,h*2);
    p=1;
  endif
  if method=="runge"
    F1 = runge(x0,a,b,h);
    F2 = runge(x0,a,b,h*2);
    p=4;
  endif
  for i = 1:1:(length(F2)-1)
    error(i) = abs(F2(2, i)-F1(2, 2*i))/(2^p-1);
  endfor
  e = max(error);
endfunction
#
function h = calculateStep(x0, a, b, e, method)
  h = abs(a-b);
  error=calculateError(x0, a, b, h, method);
  while error>e
    error=calculateError(x0, a, b, h, method);
    if error>e
      h=h/2;
    endif 
  endwhile
endfunction
#
x0=0.5;
a=1;
b=5;
h = calculateStep(x0, a, b, 0.001, "euler");
e = calculateError(x0, a, b, h, "euler");
disp("euler error");
disp(e);
A = euler(x0, a, b, h);
h = calculateStep(x0, a, b, 0.001, "runge");
e = calculateError(x0, a, b, h, "runge");
disp("runge error");
disp(e);
B = runge(x0, a, b, h);
X = a:h:b;
for i = 1:1:length(X)
  Y(i) = solution(X(i));
endfor
axis([1 2 0.4 0.6]);
plot(X,Y,'p+');
plot(A(1,:),A(2,:),'m');
plot(B(1,:),B(2,:),'k');
